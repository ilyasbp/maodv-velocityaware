# Modifikasi AODV with velocity aware
- Ilyas Bintang Prayogi
- 05111850010022
- Desain Audit Jaringan

## Sumber Paper
- Judul: New efficient velocity-aware probabilistic route discovery schemes for high mobility Ad hoc networks
- Author: Mustafa Bani Khalaf, Ahmed Y. Al-Dubai, Geyong Min.
- Tahun: 2014
- Publikasi: Elsevier
- Link: https://ieeexplore.ieee.org/document/6347633

## Penjelasan Singkat
- AODV yang memperhitungkan kecepatan node
- Modifikasi dilakukan pada fase route discovery, yaitu dengan memperhitungkan kecepatannya
- Dengan cara membagi node menjadi 2 jenis, yaitu reliable node dan unreliable node
	- Reliable node: node yang memiliki kecepatan yang mirip, sehingga aman jika dibentuk rute melalui node tsb.
	- Unreliable node: node yang memiliki kecepatan yang berbeda, sehingga berpotensi linknya rusak
- Perbedaan hak dari 2 jenis node tsb.
	- Kalau reliable node berarti dia node yang boleh membroadcast ulang RREQ yang diterimanya
	- Kalau unreliable node ya berarti tidak boleh
- Sehingga rute akan terbentuk dengan node-node yang reliable, lalu kemungkinan link rusak itu kecil

## Kode sumber untuk mengakses velocity:
	//Mendapatkan kecepatan pengirim RREQ
	double sendervx, sendervy, sendervz;
	Node* sender = Node::get_node_by_address(ih->saddr());
	((MobileNode *)sender)->getVelo(&sendervx, &sendervy, &sendervz);
	//
	//Mendapatkan kecepatan penerima RREQ
	double receivervx, receivervy, receivervz;
	Node* receiver = Node::get_node_by_address(index);
	((MobileNode *)receiver)->getVelo(&receivervx, &receivervy, &receivervz);
	//
	//Agar tidak error ketika dibagi 0
	if(sendervx==0) sendervx=0.0001;
	if(sendervy==0) sendervy=0.0001;
	if(receivervx==0) receivervx=0.0001;
	if(receivervy==0) receivervy=0.0001;
	//
	//rumus sudut teta
	double PI= 3.14159265;
	double val = 180.0 / PI;
	double pembilang = sendervx * receivervx + sendervy * receivervy;
	double penyebut = sqrt(pow(sendervx,2)+pow(sendervy,2))*sqrt(pow(receivervx,2)+pow(receivervy,2));
	double teta=acos(pembilang/penyebut)*val;
	//
	//mengecek threshold
	double tetathreshold=150;
	if(teta>tetathreshold){
		Packet::free(p); //drop packet
		return;
	} 

## Catatan
untuk mengaktifkan debug hilangkan comment pada file aodv.cc pada baris kode sebagai berikut:

	//#define DEBUG
	//#define ERROR
	#define FUNGSI
	#define ALUR


## Progress
List to do:
- [x] Buat git
- [x] Memahami paper
- [x] Modifikasi AODV
- [x] Laporan

## Referensi
- https://ieeexplore.ieee.org/document/6347633
- https://www.researchgate.net/post/How_to_obtain_the_velocity_of_a_node_in_NS2_Simulator